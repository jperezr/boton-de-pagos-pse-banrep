<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Recaudos Banco de la Republica</title>
		<!-- <link rel="stylesheet" href="css/cssIndex.css" type="text/css"></link>-->
		<!-- START @FONT STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
        <!--/ END FONT STYLES -->
        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->
        <!-- START @PAGE LEVEL STYLES -->
        <link href="assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css" rel="stylesheet">
        <link href="assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css" rel="stylesheet">
		<link href="assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->
        <!-- START @THEME STYLES -->
        <link href="assets/admin/css/reset.css" rel="stylesheet">
        <link href="assets/admin/css/layout.css" rel="stylesheet">
        <link href="assets/admin/css/components.css" rel="stylesheet">
        <link href="assets/admin/css/plugins.css" rel="stylesheet">
        <link href="assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme">
        <link href="assets/admin/css/custom.css" rel="stylesheet">
        <style>
        	.alinear-al-centro{
        		text-align: center;
        	}
        </style>
        <!--/ END THEME STYLES -->
	</head>
	<body>
	<div class="body-content animated">
		<div class="row">
            <div class="col-md-12">
                <div class="callout callout-info mb-20">
                    <p>Debes seleccionar siquiera un tipo de recaudo para continuar</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 alinear-al-centro">

            <!-- Start select fields - horizontal form -->
            <div class="panel rounded shadow no-overflow">
                
                <div class="panel-body no-padding">
                    <form action="#" class="form-horizontal form-bordered">
                        <div class="form-body">
                           

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Opciones de Recaudo</label>
                                <div class="col-sm-9">
                                    <select class="form-control input-sm mb-15" id="tipo-recaudo">
                                        <option value ="0">Seleccione tipo de Recaudo</option>
                                        <option value ="1">Pagos créditos de Cartera ( Hasta el 10 de cada mes)</option>
                                        <option value ="2">Pagos créditos de Cartera (Permanente)</option>
                                        <option value="3">Pagos de Gestión Humana ( Hasta el 10 de cada mes)</option>
                                        <option value="4">Pagos de Gestión Humana (Permanente)</option>
                                        <option value="5">Pagos servicios red de bibliotecas y museos</option>
                                        <option value="6">Pagos de servicios Administrativos</option>
                                        <option value="7">Pagos de Tesorería y Central de Efectivo</option>
                                        <option value="8">Pagos por otros conceptos</option>
                                    </select>
                                </div> 
                            </div><!-- /.form-group -->

							<div class="form-group form-group-divider">
                                <div class="form-inner">
                                    <h4 class="no-margin" id="texto-despues-seleccion">Debes Seleccionar un tipo de recaudo para diligenciar la información</h4>
                                </div>
                            </div>
                            <div class="form-body" id="tipo-1" style="display:none;">
	                            <div class="form-group">
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Concepto</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Número de Identificación (CC)</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Valor a Pagar $</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Nombre de Titular</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Email</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Nro. Crédito</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                           </div>
                     	</div>
                     	<div class="form-body" id="tipo-2" style="display:none;">
	                            <div class="form-group">
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Concepto</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Número de Identificación (CC)</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Valor a Pagar $</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Nombre de Titular</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Email</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Nro. Crédito</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                           </div>
                     	</div>
                     	<div class="form-body" id="tipo-3" style="display:none;">
	                            <div class="form-group">
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Concepto</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Número de Identificación (CC)</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Valor a Pagar $</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Nombre de Titular</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Email</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Número de teléfono</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Detalle de pago</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                           </div>
                     	</div>
                     	<div class="form-body" id="tipo-4" style="display:none;">
	                            <div class="form-group">
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Concepto</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Número de Identificación (CC)</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Valor a Pagar $</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Nombre de Titular</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Email</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Número de teléfono</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Detalle de pago</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                           </div>
                     	</div>
                     	<div class="form-body" id="tipo-5" style="display:none;">
	                            <div class="form-group">
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Concepto</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Número de Identificación (CC)</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Valor a Pagar $</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Ciudad (Sucursal)</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                           </div>
                     	</div>
                     	<div class="form-body" id="tipo-6" style="display:none;">
	                            <div class="form-group">
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Concepto</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Número de Identificación (CC)</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Valor a Pagar $</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Nombre</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Sucursal</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                           </div>
                     	</div>
                     	<div class="form-body" id="tipo-7" style="display:none;">
	                            <div class="form-group">
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Concepto</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Número de Identificación (CC/NIT)</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Valor a Pagar $</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Nombre</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Email</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Sucursal</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                           </div>
                     	</div>
                     	<div class="form-body" id="tipo-8" style="display:none;">
	                            <div class="form-group">
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Concepto</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Número de Identificación (CC/NIT)</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Valor a Pagar $</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Email</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                               <div class="input-group mb-15">
	                                   <span class="input-group-addon bg-success">Sucursal Receptora</span>
	                                   <input class="form-control no-border-left" type="text">
	                               </div>
	                           </div>
                     	</div>
                        </div><!-- /.form-body -->
                        <div class="form-footer">
                            <div class="pull-right">
                                <button class="btn btn-danger mr-5">Cancelar</button>
                                <button class="btn btn-primary" type="submit"> $ Pagar</button>
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.form-footer -->
                    </form>
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End select fields - horizontal form -->

            </div>
        </div>
		<!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- START @CORE PLUGINS -->
        <script src="assets/global/plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
        <script src="assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
        <script src="assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
        <script src="assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
        <script src="assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
        <script src="assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
        <script src="assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
        <script src="assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
        <script src="assets/global/plugins/bower_components/retina.js/dist/retina.min.js"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script src="assets/global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js"></script>
        <script src="assets/global/plugins/bower_components/holderjs/holder.js"></script>
        <script src="assets/global/plugins/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
        <script src="assets/global/plugins/bower_components/jquery-autosize/jquery.autosize.min.js"></script>
        <script src="assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js"></script>
        <!--/ END PAGE LEVEL PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="assets/admin/js/apps.js"></script>
        <script src="assets/admin/js/pages/blankon.form.element.js"></script>
        <script src="assets/admin/js/demo.js"></script>
        <!--/ END PAGE LEVEL SCRIPTS -->
        <!--/ END JAVASCRIPT SECTION -->
        <script>
			$(function(){
				//alert("entro"); tipo-recaudo texto-despues-seleccion
				$( "#tipo-recaudo" ).change(function() {
					//alert($(this).val());
					  $( "#texto-despues-seleccion" ).html("Diligenciar el formulario a continuacion");
					  if($(this).val() == '1'){
						  $( "#tipo-1" ).css("display", "none");
						  $( "#tipo-2" ).css("display", "none");
						  $( "#tipo-3" ).css("display", "none");
						  $( "#tipo-4" ).css("display", "none");
						  $( "#tipo-5" ).css("display", "none");
						  $( "#tipo-6" ).css("display", "none");
						  $( "#tipo-7" ).css("display", "none");
						  $( "#tipo-8" ).css("display", "none");
						  
						  $( "#tipo-1" ).css("display", "block");
					  }
					  if($(this).val() == '2'){
						  $( "#tipo-1" ).css("display", "none");
						  $( "#tipo-2" ).css("display", "none");
						  $( "#tipo-3" ).css("display", "none");
						  $( "#tipo-4" ).css("display", "none");
						  $( "#tipo-5" ).css("display", "none");
						  $( "#tipo-6" ).css("display", "none");
						  $( "#tipo-7" ).css("display", "none");
						  $( "#tipo-8" ).css("display", "none"); 
						  
						  $( "#tipo-2" ).css("display", "block");
					  }
					  if($(this).val() == '3'){
						  $( "#tipo-1" ).css("display", "none");
						  $( "#tipo-2" ).css("display", "none");
						  $( "#tipo-3" ).css("display", "none");
						  $( "#tipo-4" ).css("display", "none");
						  $( "#tipo-5" ).css("display", "none");
						  $( "#tipo-6" ).css("display", "none");
						  $( "#tipo-7" ).css("display", "none");
						  $( "#tipo-8" ).css("display", "none");
						  
						  $( "#tipo-3" ).css("display", "block");
					  }
					  if($(this).val() == '4'){
						  $( "#tipo-1" ).css("display", "none");
						  $( "#tipo-2" ).css("display", "none");
						  $( "#tipo-3" ).css("display", "none");
						  $( "#tipo-4" ).css("display", "none");
						  $( "#tipo-5" ).css("display", "none");
						  $( "#tipo-6" ).css("display", "none");
						  $( "#tipo-7" ).css("display", "none");
						  $( "#tipo-8" ).css("display", "none");
						  
						  $( "#tipo-4" ).css("display", "block");
					  }
					  if($(this).val() == '5'){
						  $( "#tipo-1" ).css("display", "none");
						  $( "#tipo-2" ).css("display", "none");
						  $( "#tipo-3" ).css("display", "none");
						  $( "#tipo-4" ).css("display", "none");
						  $( "#tipo-5" ).css("display", "none");
						  $( "#tipo-6" ).css("display", "none");
						  $( "#tipo-7" ).css("display", "none");
						  $( "#tipo-8" ).css("display", "none");
						  
						  $( "#tipo-5" ).css("display", "block");
					  }
					  if($(this).val() == '6'){
						  $( "#tipo-1" ).css("display", "none");
						  $( "#tipo-2" ).css("display", "none");
						  $( "#tipo-3" ).css("display", "none");
						  $( "#tipo-4" ).css("display", "none");
						  $( "#tipo-5" ).css("display", "none");
						  $( "#tipo-6" ).css("display", "none");
						  $( "#tipo-7" ).css("display", "none");
						  $( "#tipo-8" ).css("display", "none");
						  
						  $( "#tipo-6" ).css("display", "block");
					  }
					  if($(this).val() == '7'){
						  $( "#tipo-1" ).css("display", "none");
						  $( "#tipo-2" ).css("display", "none");
						  $( "#tipo-3" ).css("display", "none");
						  $( "#tipo-4" ).css("display", "none");
						  $( "#tipo-5" ).css("display", "none");
						  $( "#tipo-6" ).css("display", "none");
						  $( "#tipo-7" ).css("display", "none");
						  $( "#tipo-8" ).css("display", "none");
						  
						  $( "#tipo-7" ).css("display", "block");
					  }
					  if($(this).val() == '8'){
						  $( "#tipo-1" ).css("display", "none");
						  $( "#tipo-2" ).css("display", "none");
						  $( "#tipo-3" ).css("display", "none");
						  $( "#tipo-4" ).css("display", "none");
						  $( "#tipo-5" ).css("display", "none");
						  $( "#tipo-6" ).css("display", "none");
						  $( "#tipo-7" ).css("display", "none");
						  $( "#tipo-8" ).css("display", "none");
						  
						  $( "#tipo-8" ).css("display", "block");
					  }
					  if($(this).val() == '0'){
						  $( "#texto-despues-seleccion" ).html("Debes Seleccionar un tipo de recaudo para diligenciar la información");
						  $( "#tipo-1" ).css("display", "none");
						  $( "#tipo-2" ).css("display", "none");
						  $( "#tipo-3" ).css("display", "none");
						  $( "#tipo-4" ).css("display", "none");
						  $( "#tipo-5" ).css("display", "none");
						  $( "#tipo-6" ).css("display", "none");
						  $( "#tipo-7" ).css("display", "none");
						  $( "#tipo-8" ).css("display", "none");
					  }
					});
			});
        </script>
    </div>    
	</body>
</html>